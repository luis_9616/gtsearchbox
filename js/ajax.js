$(obtener_registros());
var fechaHoy;

function obtener_registros(detinations_by_suppliers) {
  $.ajax({
    url: 'core/ajax.php',
		type: 'POST',
		dataType: 'html',
		data: { detinations_by_suppliers: detinations_by_suppliers },
    success: function(resultado) {
		$(".tabla_resultado").html(resultado);
    },
    error: function() {
    //   console.log("No se ha podido obtener la información");
    }
  });
}

// ===============================================================
// funcion que simula la prediccion en tiempo real de los destinos
// ===============================================================
$(document).on('keyup', '.bxSearch', function(){
	var valorBusqueda=$(this).val();
	if (valorBusqueda!="")
	{
		obtener_registros(valorBusqueda);
	}
	else
		{
			obtener_registros();
		}
});

// =============================================================================================================
// obtiene el resultado seleccionado por el usuario lo asinga al input destino y oculta el select de la busqueda
// =============================================================================================================
$(document).on('change', '#selectResult', function(event) {
	$('#destinyName').val($("#selectResult option:selected").text());
	$('#selectResult').hide();
});

// =====================================================================
// funcion que traer el lenguaje en el que esta la pagina en ese momento
// =====================================================================
$(document).ready(function(){
	var langx = window.location.pathname;
		$('#arrayLang').val((langx));
		setDates();
		$('#checkIn').attr("min", fechaHoy);
});

// ============================================================
// funcion que agrega las fechas al momento de cargar la pagina 
// ============================================================
function setDates() {
		var fecha = new Date(); //Fecha actual
		var m = fecha.getMonth()+1; //obteniendo mes
		var d = fecha.getDate(); //obteniendo dia
		var y = fecha.getFullYear(); //obteniendo año
		if(d<10){
			d='0'+d; //agrega cero si el menor de 10
		}
		if(m<10){
			m='0'+m //agrega cero si el menor de 10
		}		
		$('#checkIn').val(y+"-"+m+"-"+d);
		fechaHoy = y+"-"+m+"-"+d;
		
		// canbia el value de checkOut
		changeIn();
}

// ==========================================================================================
// funcion que esta al pendiente de los cambios en el checkIn y asi poder cambiar el checkOut
// ==========================================================================================
$(document).on('change', '#checkIn', function() {
	if ($('#checkOut').val() < $('#checkIn').val()) {
		changeIn();
	}
});

// ==========================================================================================
// funcion que esta al pendiente de los cambios en el checkOut y asi poder cambiar el checkIn
// ==========================================================================================
$(document).on('change', '#checkOut', function() {
	if ($('#checkOut').val() < $('#checkIn').val()) {
		changeOut();
	}
});

// ===========================================================
// funcion para cambiar el checkOut si el checkIn fue cambiado
// ===========================================================
function changeIn() {
	let salida = new Date($('#checkIn').val());
	let masDias = 2;
	
	salida.setDate(salida.getDate() + masDias);
	var m1 = salida.getMonth()+1; //obteniendo mes
	var d1 = salida.getDate(); //obteniendo dia
	var y1 = salida.getFullYear(); //obteniendo año
	if(d1<10){
			d1='0'+d1; //agrega cero si el menor de 10
		}
		if(m1<10){
			m1='0'+m1 //agrega cero si el menor de 10
		}
		$('#checkOut').val(y1+"-"+m1+"-"+d1);
}
	
// ===========================================================
// funcion para cambiar el checkIn si el checkOut fue cambiado
// ===========================================================
function changeOut() {
	let salida = new Date($('#checkOut').val());
	let masDias = 0;
	
	salida.setDate(salida.getDate() - masDias);
	var m1 = salida.getMonth()+1; //obteniendo mes
	var d1 = salida.getDate(); //obteniendo dia
	var y1 = salida.getFullYear(); //obteniendo año
	if(d1<10){
		d1='0'+d1; //agrega cero si el menor de 10
	}
	if(m1<10){
		m1='0'+m1 //agrega cero si el menor de 10
	}
	$('#checkIn').val(y1+"-"+m1+"-"+d1);
}