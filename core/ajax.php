<?php
include('./connection.php');

$tabla="";
$query="SELECT * FROM detinations_by_suppliers ORDER BY id";

///////// LO QUE OCURRE AL TECLEAR SOBRE EL INPUT DE BUSQUEDA ////////////
if(isset($_POST['detinations_by_suppliers']))
{
	$q=$conexion->real_escape_string($_POST['detinations_by_suppliers']);
	$query="SELECT * FROM detinations_by_suppliers WHERE
		parent LIKE '%".$q."%' OR
		city_en LIKE '%".$q."%' OR
		city_default LIKE '%".$q."%' OR
		city_es LIKE '%".$q."%'";
}

$buscarB=$conexion->query($query);
// if ($buscarB->num_rows > 0)
if ($buscarB != false)
{
	if ($buscarB->num_rows > 0) {	
		$tabla.=
		'<select name="datSupplier" class="col col-12 form-control" multiple id="selectResult">';
		
		while($filaB= $buscarB->fetch_assoc())
		{
			// =========================================================================================
			// condiciona el idioma para mostrar a uno solo para no tener redundancia con los resultados
			// =========================================================================================
			if ($filaB['city_es'] != 'idioma no disponible') {
				$lang = $filaB['city_es'];
			} elseif ($filaB['city_en'] != 'language not available'){
				$lang = $filaB['city_en'];
			}else{
				$lang = $filaB['city_default'];
			}

			// $qx=$conexion->real_escape_string($filaB['id_supplier']);
			$auxQuery = "SELECT id_prov, supplier_code, access_code FROM suppliers WHERE id_prov = '".$filaB['id_supplier']."'";
			$aB=$conexion->query($auxQuery);
			$id_sup = $aB->fetch_assoc();

			// $qz=$conexion->real_escape_string($filaB['id_destinations']);
			$aQuery = "SELECT destination_Leaf, closes_destinations FROM codes_destinations WHERE id_destinations = '".$filaB['id_destinations']."'";
			$aC=$conexion->query($aQuery);
			$codez = $aC->fetch_assoc();
			
			// $qz=$conexion->real_escape_string($filaB['id_destinations']);
			$currency = "SELECT USD, EUR FROM trvl_currency_change WHERE id_currency = 1";
			$aX=$conexion->query($currency);
			$coins = $aX->fetch_assoc();	
			
			// $tabla.='<option class="typeMax" value="'.$filaB["id_destinations"].'">'.$lang.' '.$filaB['available'].' '. $filaB['parent'].' '.$filaB['type'].' '.$filaB['id_destinations'].' supplier: '.$id_sup['supplier_code']. '</option>';
			$tabla.="<option class='typeMax' value='".json_encode(array('id_destiny' => $filaB['id_destinations'],'id_supplier' => $filaB['id_supplier'],'sup_code' =>$id_sup['supplier_code'],'sup_access' =>$id_sup['access_code'],'leaf'=>$codez['destination_Leaf'], 'closes'=>$codez['closes_destinations'],'usd'=>$coins['USD'],'eur'=>$coins['EUR']))."'>".$lang." ". $filaB['parent']." ".$filaB['type']." ".$filaB['id_destinations']." supplier: ".$id_sup['supplier_code']. "</option>";
		}
		$tabla.='</select>';
	}else{
		$tabla .= '<div class="alert alert-danger col-sm-12 col-md-12 col-lg-9" role="alert" style="position:absolute; bottom: -6.5rem !important; left:1rem; z-index:3;">
		<h4 class=" alert-headding alert-link"><i class="fas fa-exclamation-triangle"></i> Error!</h4>
		<span class="">No se encontraron coincidencias</span>
	  </div>';
	}
}else{
	// $tabla = '';
}
echo $tabla;
	// para la version 2 se debe de hacer que se envie un array con los datos del destino elegido asi como los datos del proveedor que ofrece este destino
			// $city = array();
			// $zone = array();
			// para condicionar que se muestre por tipo de zona o ciudad fase beta
			// if ($filaB['type'] == 'ZONE') {
			// 	$tabla.='<optgroup class="typeMin" label=Zona>';
			// 	$tabla.='<option class="typeMax" value="'.$filaB["id_destinations"].'">'.$lang.' '.$filaB['available'].' '. $filaB['parent'].' '.$filaB['type'].' '.$filaB['id_destinations'].' supplier: '.$id_sup['supplier_code']. '</option>';
			// 	$tabla.='</optgroup>';

			// }elseif ($filaB['type'] == 'CITY') {
			// 	$tabla.='<optgroup class="typeMin" label=Ciudad>';
			// 	$tabla.='<option class="typeMax" value="'.$filaB["id_destinations"].'">'.$lang.' '.$filaB['available'].' '. $filaB['parent'].' '.$filaB['type'].' '.$filaB['id_destinations'].' supplier: '.$id_sup['supplier_code']. '</option>';
			// 	$tabla.='</optgroup>';
			// }		
			// $tabla.='<option value="'.$filaB["id_destinations"].'">'.$lang.' '.$filaB['available'].' '. $filaB['parent'].' '.$filaB['type'].' '.$filaB['id_destinations'].' supplier: '.$id_sup['supplier_code']. '</option>';
			// '<option value="">'.$auxLan1.' '.$filaB['type']. ' '.$filaB['available']. ' '.$auxLan2.'</option>';
?>