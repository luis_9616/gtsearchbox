<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Go Tour</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/movil.css">
    <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="./dist/css/bootstrap-select.min.css"> -->
    <!-- kit's font awesome icons -->
    <script src="https://kit.fontawesome.com/63e2dea6dc.js"></script>
    
    <!-- Links & scripts para calendario -->
    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="css/calendar.css">
    <script src="js/ajax.js"></script>
    <!-- jquery iu para el calendario -->
    <style>
    .typeMin{
        font-size:10px;
    }
    .typeMax{
        font-size:18px;
    }
    </style>
</head>
<body style="background:#333">
    <header>
        <figure>
            <!-- <a href="index.php"><img src="img/logo.png" alt=""></a> -->
        </figure>
    </header>
<section class="container">
        <section class="menu_hotel row">
            <h2 class="titlex">Hoteles</h2>
            <?php include("core/connection.php"); ?>
            <!-- <form class="formMy col col-12 form-group"  action="http://travelgatemodule.u/recibir.php" method="POST">             -->
            <form class="formMy col col-12 form-group" target="_blank" action="http://travelgatemodule.u/searches.php" method="POST">            
            <!-- <form class="formMy col col-12 form-group"  action="http://gotourmarque.u/recibir.php" method="POST">             -->
            <!-- ../travelgatemodule/examples/query_search.php -->
            <!-- lenguaje del sistema -->
            <input id="arrayLang" name="langPage" type="hidden">
                <div id="lorem" class="form-group col-sm-12 col-md-12 col-lg-9">
                    <label class="labelMin" for="busqueda">Destino</label>
                    <i class="innerIcon fas fa-search-location"></i>
                    <!-- nombre del destino -->
                    <input id="destinyName"
                    class="bxSearch form-control form-control-lg" 
                    type="text"
                    class="form-control" 
                    placeholder="Buscar..." 
                    autocomplete="off" 
                    name="busqueda"
                    required>       
                    <!-- SELECT PARA TRAER LOS DESTINOS QUE TENGO EN LA BASE DE DATOS -->
                    <div class="tabla_resultado"></div>
                    <!-- SELECT PARA TRAER LOS DESTINOS QUE TENGO EN LA BASE DE DATOS -->
                </div>
                <div class="aaa col-sm-6 col-md-6 col-lg-4">
                    <label class="labelMin labelMin2" for="checkIn">Check In</label>
                    <i class="innerIcon fas fa-calendar-day"></i>
                    <!-- fecha de entrada -->
                    <input type="date" 
                           class="form-control checks" 
                           name="checkIn" 
                           id="checkIn" 
                           required>
                </div>

                <div class="aaa col-sm-6 col-md-6 col-lg-4">
                    <label class="labelMin labelMin2" for="checkOut">Check Out</label>
                    <i class="innerIcon fas fa-calendar-day"></i>
                    <!-- fecha de salida -->
                    <input type="date" 
                           class="form-control checks" 
                           name="checkOut" 
                           id="checkOut"
                           required>
                </div>
                <div class="aaa col-sm-6 col-md-6 col-lg-6">
                    <label class="labelMin" for="rooms">Numero de habitaciones</label>
                    <i class="innerIcon fas fa-bed"></i>
                    <!-- define el numero de habitaciones -->
                    <Select name="rooms" class=" selPad checks form-control" required>
                        <option value="1" selected>1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                    </Select>
                </div>
                <div class="w-100"></div>
                <div class="form-group">
                    <input class="btn btn-dark btn-lg col-12 m-3" type="submit" value="Buscar">
                </div>
            </form>
        </section>
        
    </section>
    <div class="contenedor1">
        <!-- <nav>
            <ol>
                <li><a href="verano.php" target="info">VERANO</a></li>
                <li><a href="familias.php" target="info">FAMILIAS</a></li>
                <li><a href="ultimahora.php" target="info">ULTIMA HORA</a></li>
                <li><a href="finesdesemana.php" target="info">FINES DE SEMANA</a></li>
                <li><a href="todoincluido.php" target="info">TODO INCLUIDO</a></li>
            </ol>
        </nav> -->
		<hr>
		<div id="info">
			<!-- <iframe src="verano.php" name="info" frameborder="0" scrolling="no"></iframe> -->
		</div>
		
    </div>
    <footer>
    <div class="waves">
        <div class="index_waves wave1"></div>
        <div class="index_waves wave2"></div>
        <div class="index_waves wave3"></div>
    </div>
    </footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<!-- <script src="./dist/js/bootstrap-select.min.js"></script> -->
</body>
</html>